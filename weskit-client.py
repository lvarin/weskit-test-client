#!/usr/bin/env python3

import os
import sys
import json

import requests
from requests.exceptions import JSONDecodeError, ConnectionError
import yaml

weskit_host = "https://weskit-test.rahtiapp.fi"
#weskit_host = "https://weskit-test2.rahtiapp.fi"

with open("config-SMK.yaml") as file:
    workflow_params_SMK = json.dumps(yaml.load(file, Loader=yaml.FullLoader))

with open("config-NFL.yaml") as file:
    workflow_params_NFL = json.dumps(yaml.load(file, Loader=yaml.FullLoader))

reference_data = {
    "SMK":
    {
        "workflow_params": workflow_params_SMK,
        "workflow_type": "SMK",
        "workflow_type_version": "6.10.0",
        "workflow_url": "Snakefile"
    },
    "NFL":
    {
        "workflow_params": workflow_params_NFL,
        "workflow_type": "NFL",
        "workflow_type_version": "21.04.0",
        "workflow_url": "helloworld.nf"
    }
}

reference_files = {
    "SMK": [
        ("workflow_attachment", ("Snakefile", open("Snakefile", "rb"))),
        ("workflow_attachment", ("bwa_samtools_bcftools.yaml", open("bwa_samtools_bcftools.yaml", "rb"))),
        ("workflow_attachment", ("py_plot.yaml", open("py_plot.yaml", "rb"))),
        ("workflow_attachment", ("plot-quals.py", open("plot-quals.py", "rb")))
    ],
    "NFL": [
        ("workflow_attachment", ("helloworld.nf", open("helloworld.nf", "rb")))
    ]
}
#####################################################################

try:
    if os.environ['DEBUG']:
        DEBUG = True
except KeyError:
    DEBUG = False

print(" * Using: %s as host" % weskit_host)

try:
    wtype = sys.argv[1]
except IndexError:
    wtype = 'SMK'

try:
    run_id = sys.argv[2]
except IndexError:

    try:
        data = reference_data[wtype]
    except KeyError:
        print("Choose between %s" % reference_data.keys())
        exit(2)

    files = reference_files[wtype]

    if DEBUG:
        print("DATA:\n%s" % json.dumps(data, indent=4))
        print("FILES: %s" % files)
    try:
        response = requests.post("%s/ga4gh/wes/v1/runs" % (weskit_host), data=data, files=files)
    except ConnectionError:
        print("ERROR: Cannot connect to %s, exiting" % weskit_host)
        exit(1)

    print("Code: %d" % response.status_code)

    try:
        print("%s" % json.dumps(response.json(), indent=4))
    except JSONDecodeError as json_err:
        print("ERROR: Cannot decode JSON output: %s" % response.text)

    if response.status_code != 200:
        exit(1)

    run_id = response.json()['run_id']

run = requests.get("%s/ga4gh/wes/v1/runs/%s" % (weskit_host, run_id))
print("Code: %d" % run.status_code)
print("%s" % json.dumps(run.json(), indent=4))
