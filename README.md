# WEskit TEST client

This is a test client for the weskit server. It is a copy and adaptation of the client in:

* https://gitlab.com/twardzso/espace_eucancan_training_snakemake/-/tree/master/2_cloud

The workflow has to be already present into the server in `/weskit/workflows`. The files can be found also in the above link in gitlab.

## Run the client

```
$ ./weskit-client.py
Code: 200
{
    "run_id": "46713212-6f6f-48b4-a525-6891734b9c50"
}
Code: 200
{
    "outputs": {},
    "request": {
        "workflow_params": {
            "genome": "/data/reference/genome.fa",
            "samples": {
                "A": "/data/protected/samples/A.fastq",
                "B": "/data/protected/samples/B.fastq",
                "C": "/data/protected/samples/C.fastq"
            }
        },
        "workflow_type": "SMK",
        "workflow_type_version": "6.10.0",
        "workflow_url": "Snakefile"
    },
    "run_id": "46713212-6f6f-48b4-a525-6891734b9c50",
    "run_log": {
        "cmd": [
            "snakemake",
            "--snakefile",
            "../../../weskit/workflows/Snakefile",
            "--cores",
            "1",
            "--configfile",
            "config.yaml"
        ],
        "env": {}
    },
    "state": "QUEUED",
    "task_logs": [],
    "user_id": "not-logged-in-user"
}
```

Then to check the status of a run:

```
$ ./weskit-client.py 46713212-6f6f-48b4-a525-6891734b9c50                                   
Code: 200
{
    "outputs": {
        "workflow": [
            "config.yaml",
            ".snakemake/log/2022-05-11T054518.734353.snakemake.log"
        ]
    },
    "request": {
        "workflow_params": {
            "genome": "/data/reference/genome.fa",
            "samples": {
                "A": "/data/protected/samples/A.fastq",
                "B": "/data/protected/samples/B.fastq",
                "C": "/data/protected/samples/C.fastq"
            }
        },
        "workflow_type": "SMK",
        "workflow_type_version": "6.10.0",
        "workflow_url": "Snakefile"
    },
    "run_id": "46713212-6f6f-48b4-a525-6891734b9c50",
    "run_log": {
        "cmd": [
            "snakemake",
            "--snakefile",
            "../../../weskit/workflows/Snakefile",
            "--cores",
            "1",
            "--configfile",
            "config.yaml"
        ],
        "end_time": "2022-05-11T05:45:18Z",
        "env": {},
        "exit_code": 1,
        "log_dir": ".weskit/2022-05-11T05:45:18Z",
        "log_file": ".weskit/2022-05-11T05:45:18Z/log.json",
        "output_files": [
            "config.yaml",
            ".snakemake/log/2022-05-11T054518.734353.snakemake.log"
        ],
        "start_time": "2022-05-11T05:45:18Z",
        "stderr_file": ".weskit/2022-05-11T05:45:18Z/stderr",
        "stdout_file": ".weskit/2022-05-11T05:45:18Z/stdout",
        "workdir": "4671/46713212-6f6f-48b4-a525-6891734b9c50"
    },
    "state": "COMPLETE",
    "task_logs": [],
    "user_id": "not-logged-in-user"
}
```

## Server installation

We used the helm chart here:

* https://gitlab.com/zagganas/deployment.git

And adapted few things. First a BuildConfig to make a new image is added, OpenShift will build and deploy this image automatically. Secondly an emptyDir is added to redis. so it can write in the `/data` forder. Third an adapted `Dockerfile` is used.

The current Helm chart that works for OpenShift is:

https://gitlab.com/lvarin/weskit-deployment (in `fixes` branch)

And the source for the new image is:

https://gitlab.com/lvarin/weskit-api


